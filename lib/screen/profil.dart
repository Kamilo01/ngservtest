import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Profil extends StatefulWidget {
  const Profil({Key? key}) : super(key: key);

  @override
  _ProfilState createState() => _ProfilState();
}

class _ProfilState extends State<Profil> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Color(0xff009858),
        elevation: 12.0,
        title: Text("Mon profil"),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.account_circle),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.notifications),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            child: Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: 200.0,
                  decoration: BoxDecoration(
                    color: Color(0xff009858),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 60,
                          backgroundColor: Color(0xffFF9800),
                          child: CircleAvatar(
                            radius: 58,
                            backgroundColor: Color(0xff009858),
                            child: CircleAvatar(
                              radius: 50,
                              backgroundColor: Colors.white,
                              child: CircleAvatar(
                                radius: 45,
                                backgroundColor: Colors.transparent,
                                backgroundImage:
                                    AssetImage('assets/images/mucrefci.png'),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 8.0),
                        Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "teamsa".toUpperCase(),
                                style: TextStyle(
                                    fontSize: 35.0,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400),
                              ),
                              Text(
                                "test".toUpperCase(),
                                style: TextStyle(
                                  fontSize: 25.0,
                                  color: Color(0xffFF9800),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 180.0,
                  decoration: BoxDecoration(
                    border: Border(
                      bottom:
                          BorderSide(width: 3.0, color: Colors.grey.shade300),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      children: [
                        Row(children: [
                          Text(
                            "Infos personnelles",
                            style: TextStyle(
                              fontSize: 25.0,
                              color: Color(0xff009858),
                            ),
                          )
                        ]),
                        SizedBox(height: 25.0),
                        Row(
                          children: [
                            Text(
                              "Numéro Téléphone : ",
                              style: TextStyle(fontWeight: FontWeight.w500),
                            ),
                            Text(
                              "0708863980",
                              style: TextStyle(fontWeight: FontWeight.w300),
                            ),
                          ],
                        ),
                        SizedBox(height: 5.0),
                        Row(
                          children: [
                            Text(
                              "Addresse Email  : ",
                              style: TextStyle(fontWeight: FontWeight.w500),
                            ),
                            Text(
                              "auguste.soro@ngser.com",
                              style: TextStyle(fontWeight: FontWeight.w300),
                            ),
                          ],
                        ),
                        SizedBox(height: 5.0),
                        Row(
                          children: [
                            Text(
                              "Date de Naissance : ",
                              style: TextStyle(fontWeight: FontWeight.w500),
                            ),
                            Text(
                              "31-12-1992",
                              style: TextStyle(fontWeight: FontWeight.w300),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 200.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                      bottom:
                          BorderSide(width: 3.0, color: Colors.grey.shade300),
                    ),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Column(children: [
                      Row(
                        children: [
                          Text(
                            "Editer mes infos",
                            style: TextStyle(
                              fontSize: 25.0,
                              color: Color(0xff009858),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 20.0),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Icon(
                              Icons.edit,
                              color: Color(0xffFF9800),
                            ),
                            Text(
                              "Editer mes infos persos",
                              style: TextStyle(
                                  fontSize: 18.0, fontWeight: FontWeight.w500),
                            ),
                            Icon(
                              Icons.arrow_forward_ios_outlined,
                              color: Color(0xff009858),
                            )
                          ]),
                      Container(
                        width: double.infinity,
                        height: 15.0,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                            bottom: BorderSide(
                                width: 3.0, color: Colors.grey.shade300),
                          ),
                        ),
                      ),
                      SizedBox(height: 15.0),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Icon(
                              Icons.lock_outlined,
                              color: Color(0xffFF9800),
                            ),
                            Text(
                              "Editer mon mot de passe",
                              style: TextStyle(
                                  fontSize: 18.0, fontWeight: FontWeight.w500),
                            ),
                            Icon(
                              Icons.arrow_forward_ios_outlined,
                              color: Color(0xff009858),
                            )
                          ]),
                      Container(
                        width: double.infinity,
                        height: 15.0,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                            bottom: BorderSide(
                                width: 3.0, color: Colors.grey.shade300),
                          ),
                        ),
                      ),
                    ]),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
