class User {
  late int userId;
  late String userName;
  late String userLastName;
  late String userBirthdate;
  late String userEmail;
  late String userPhoneNumber;
  late String token;
  late String renewalToken;

  //constructor
  User(
      {required this.userId,
      required this.userName,
      required this.userLastName,
      required this.userEmail,
      required this.userPhoneNumber,
      required this.userBirthdate,
      required this.token,
      required this.renewalToken});

  //Json serialization
  factory User.fromJson(Map<String, dynamic> responseData) {
    return User(
        userId: responseData['id'] as int,
        userName: responseData['name'] as String,
        userLastName: responseData['lastname'] as String,
        userEmail: responseData['email'] as String,
        userPhoneNumber: responseData['phone'] as String,
        userBirthdate: responseData['brithdate'],
        token: responseData['access_token'],
        renewalToken: responseData['renewal_token']);
  }
}
